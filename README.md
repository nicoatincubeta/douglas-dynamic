# Steps in creating resizes

- Change `container_dc` in the css to the size of your creative.
- Update animation values on `startFrame_1` function particularly the `.frame1 .layer_top` height animation.
- Update `.frame2 .layer_top` height in the css to use the same height value of `.frame1 .layer_top` in the `startFrame_1` animation function.
- Update douglas logo width and height in `.frame1 .logo` in the css
- Update `.frame1 .bottomPanel` height in the css.
- Update height of `.frame1 .layer_bottom` to the height of the creative (_minus header height on vertical units_).
- Update `.frame1 .ctaBtn` width and height values in the css.
- Update CTA settings in `a1_cta__json` in the invocation code.
- Update `.frame1 .mainText` values in the css.
- Update background image size based on the creative dimensions.
- Update `.frame1 .imageCover` height in the css (_gradient overlay on background image_).

# Douglas Dynamic Layout Guide

## Option A (with brand logo - *default layout*)
*with bottom bar and brand logo (for box units)*
*with side bar and brand logo (for rect units)*

---
- Header (ie: Discover Our)
- Subheader (ie: Skin Care Innovations)
- Bottom Panel (ie: White Box)
  - Brand Logo
  - CTA

## Variable Settings

- `a1_showBrandLogo__select = "yes";`
- `a1_bottomBar__json.show__select = "yes"`

---

## Option A (without brand logo)
*with bottom bar and no brand logo (for box units)*
*no side bar and no brand logo (for wide rect units)*

---
- Header (ie: Discover Our)
- Subheader (ie: Skin Care Innovations)
- Bottom Panel (ie: White Box)
  - CTA

## Variable Settings

- `a1_showBrandLogo__select = "no"`
- `a1_bottomBar__json.show__select = "no"`
- Polish alignment manually inside `politeInit()` function.

---

## Option B (with brand logo)
*no bottom bar, no header, and with brand logo (for box units)*
*no side bar, no header, and with brand logo (for rect units)*

---

- Subheader (ie: Skin Care Innovations)
- Brand Logo
- CTA

## Variable Settings

- `a1_headline__json.value = ""`
- `a1_showBrandLogo__select = "yes"`
- `a1_bottomBar__json.show__select = "no"`
- Polish alignment of elements manually inside `politeInit()` function.

---

## Option B (without brand logo)
*no bottom bar and no brand logo (for box units)*
*no side bar and no brand logo (for rect units)*

---

- Subheader (ie: Skin Care Innovations)
- Brand Logo
- CTA

## Variable Settings

- `a1_showBrandLogo__select = "no"`
- `a1_bottomBar__json.show__select = "no"`
- Polish alignment manually inside `politeInit()` function.

---

# Updating the carousel to wide sizes.

- Update values of `.carousel_main` in the css based on the creative dimensions.
- Update `car_item_height` in js to be the same value as `.carousel_main` in the css.
- Copy animation code block for  `.frame1 .layer_top` to `startFrame_2` function.
- Update positioning of `.carbrand` (_the product image_) in the css.
- Update positioning and font size of `.description` in the css.
- Update positioning and font size of `.specs` in the css.
- Update positioning and font size of `.brandLogo` in the css.
- Update positioning and font size of `.price` in the css.
- Update positioning and font size of `.basePrice` in the css.
- Update positioning and font size of `.discount_circle, .discount_number` in the css.
- Update positioning and font size of `.frame2 .ctaBtn` in the css.
- Move `.nav_left` and `.nav_right` HTML codes inside `.carousel_main`.
- Disable `.ctaBtn` HTML code in frame 2. This is because wide sizes needs individual cta buttons for the product.
- Disable all `setCta` function call on frame 2 cta (https://take.ms/5pLWz).
- Copy the `.ctaBtn` HTML code and paste it inside "setup carousel items" section (https://take.ms/sAgrG).
- Create a new `forEach` code block to get the cta button config from the invocation code and apply it to all the individual cta of the products (https://take.ms/NSpOA).


---

# Code Line Numbers (for faster editing)

- Frame 1 #182
- Frame 2 #308
- Carousel CSS #458
- Invo Code #662
- Animation Code #1094
- Dynamic Setup #915

---

# Creating 02-audience-last-viewed variant

- Remove all `.frame1` styles in the CSS.
- Replace all `.frame2` styles to `.frame1` in the CSS.
- Remove all `a1_xxx` references.
- Replace all `a2_xxx` references to `a1_xxx`.
- Remove all `.frame1` references (https://take.ms/MWIU9) except for (https://take.ms/gSZrK).
- Remove `startFrame_2` function.
- Updated `startFrame_1` function.
